package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final EmployeeRepository employeeRepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, EmployeeRepository employeeRepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeRepository = employeeRepository;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public List<Employee> findAll() {
        return getEmployeeRepository().findAll();
    }

    public Employee findById(Long id) {
        return getEmployeeRepository().findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        create(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return getEmployeeRepository().findAllByGender(gender);
    }

    public Employee create(Employee employee) {
        return getEmployeeRepository().save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return getEmployeeRepository().findAll(PageRequest.of(page - 1, size)).toList();
    }

    public void delete(Long id) {
        getEmployeeRepository().deleteById(id);
    }
}
