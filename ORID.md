O:
Today, I mainly learned some basic syntax of Sql and the JPA framework. JPA and Mybatis are similar in that they mainly provide a set of interfaces for manipulating databases, which can be compatible with different databases, making it easier for developers to develop.
R:
relatively-easy
I:
Previously, I have been using Mybatis and have not been exposed to JPA. I feel that JPA is very similar to Mybatis plus, providing many APIs without the need to implement SQL statements myself. However, this brings about a problem of low degree of freedom and a bit of discomfort. Another problem, in today's exercise, the password for the database was written in the configuration file. If pushed to Gitlab, it would not be very secure to expose the password. However, the previous commit information included this password, so it is a bit troublesome. Now can only recreate the branch to solve this problem or change the password.
D:
Find a time to search for some JPA documents and learn relevant APIs.